package com.silvanacorrea.tarea05

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        llenarSpinner()
        btnConfirmar.setOnClickListener {


            val nombreTitular = edtNombre.text.toString()
            val marcaSeleccionada = spMarca.selectedItem.toString()
            val annioAuto = edtAnnio.text.toString()
            val descripcion = edtDescripcion.text.toString()

            if (nombreTitular.isEmpty()){
                Toast.makeText(this, "Debe ingresar nombre del titular", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (annioAuto.isEmpty()){
                Toast.makeText(this, "Debe ingresar el año de auto", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (descripcion.isEmpty()){
                Toast.makeText(this, "Debe ingresar la descripción del problema", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val auto = Auto(nombreTitular,marcaSeleccionada,annioAuto,descripcion)

           /* println(auto.nombre)
            println(auto.marca)
            println(auto.annio)
            println(auto.descripcion)*/


           // Toast.makeText(this, "Titular: ${auto.nombre} Marca: ${auto.marca} Año: ${auto.annio} Descripción: ${auto.descripcion}",Toast.LENGTH_SHORT).show()
           // clear()

           val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setMessage("Titular: ${auto.nombre} \n Marca: ${auto.marca} \n Año: ${auto.annio} \n Descripción: ${auto.descripcion}")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", DialogInterface.OnClickListener{ dialog, id -> clear()  })
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener {
                        dialog, id -> dialog.cancel()})

            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("Datos registrados")
            // show alert dialog
            alert.show()

        }    }

    fun llenarSpinner(){

        var marcas = mutableListOf("Peugeot","Volkswagen","seat","Reanault")

        val adaptador =  ArrayAdapter(this, R.layout.style_item,marcas)
        spMarca.adapter = adaptador

    }

    fun clear(){
        edtNombre.setText("")
        edtAnnio.setText("")
        edtDescripcion.setText("")
    }



}